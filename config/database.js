require('dotenv').config()

const DB_HOST = process.env['DB_HOST']
const DB_PORT = '3306'
const DB_DATABASE = process.env['DB_DATABASE']
const DB_USERNAME = process.env['DB_USERNAME']
const DB_PASSWORD = process.env['DB_PASSWORD']

var mysql = require('mysql');
var config = require('./config')

class DB {
    /**
     * Setup database credentials
     */
    constructor(name) {
        try {
            this.pool = mysql.createPool({
                connectionLimit: 10,
                host: DB_HOST,
                user: DB_USERNAME,
                password: DB_PASSWORD,
                database: DB_DATABASE
            })
        } catch (error) {
            let msg = 'Unable to connect to dtabase please check database connection'
            let status = 'Fails'
        }

    }

    executeQuery(query, parameters=[]) {
        var that = this;
        return new Promise(function (resolve, reject) {
            let response = {
                'status': config.status.FAIL,
                'data': null
            }
            try {
                that.pool.getConnection(function (err, connection) {
                    if (err) {
                        resolve(response);
                    } else {
                        connection.query(query, parameters, function (error, results, fields) {
                            connection.release();
                            if (error) response['data'] = error;
                            else {
                                response['status'] = config.status.SUCCESS;
                                response['data'] = results;
                            }
                            resolve(response);
                        });
                    }
                });
            } catch (error) {
                response['data'] = error
                resolve(response);
            }
        });
    }

    async close() {
        await this.pool.end()
    }

}

module.exports = DB