module.exports = {
  status: {
    FAIL: "FAIL",
    ERROR: "ERROR",
    WARNING: "WARNING",
    SUCCESS: "SUCCESS"
  },
};