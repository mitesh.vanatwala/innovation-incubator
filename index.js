const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');
const config = require("./config/config");
const user_service = require('./src/user_service');
const { body, validationResult } = require('express-validator');
const multer = require('multer');
const csv = require('fast-csv');
const fs = require('fs');
const Queue = require('bull');
const { get } = require("lodash");
require('dotenv').config()

global.__basedir = __dirname;

const app = express();
const port = 3000;

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const sendMailQueue = new Queue('send-mail', process.env['REDIS_URL']);
const parkingLotQueue = new Queue('parking-lot-queue', process.env['REDIS_URL']);
const userServiceObject = new user_service();

sendMailQueue.process(async function (job, done) {
  const { emailId, newsletterName, newsletterContent } = get(job, 'data', {});
  const { status } = await userServiceObject.sendNewsletter(emailId, newsletterName, newsletterContent);
  if (status !== config.status.SUCCESS) {
    parkingLotQueue.add({
      emailId,
      newsletterName,
      newsletterContent
    });
  }
  done();
});

parkingLotQueue.process(async function (job, done) {
  // console.log(job)  
});

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, __basedir + '/uploads/')
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname)
  }
});

// Filter for CSV file
const csvFilter = (req, file, cb) => {
  if (file.mimetype.includes("csv")) {
    cb(null, true);
  } else {
    cb("Please upload only csv file.", false);
  }
};
const upload = multer({ storage: storage, fileFilter: csvFilter });

app.post(
  '/add-user',
  body('emailid').isEmail(),
  body('firstname').isLength({ min: 2 }),
  body('lastname').isLength({ min: 2 }),
  body('age').isFloat(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: config.status.ERROR, errors: errors.array() });
    }
    const { firstname, lastname, emailid, age } = req.body;
    const addUserResponse = await userServiceObject.addUser(firstname, lastname, emailid, age);
    res.send(addUserResponse);
});

app.post(
  '/send-newsletter',
  upload.single("file"),
  async (req, res) => {
    if (req.file == undefined) {
      return res.status(400).json({
        status: config.status.ERROR,
        error: "Please upload a CSV file!"
      });
    }
    
    let filePath = __basedir + '/uploads/' + req.file.filename;
    fs.createReadStream(filePath)
      .pipe(csv.parse({ headers: false }))
      .on("error", (error) => {
        throw error.message;
      })
      .on("data", (row) => {
        const emailId = get(row, '[0]', '');
        const newsletterName = get(row, '[1]', '');
        const newsletterContent = get(row, '[2]', '');
        sendMailQueue.add({
          emailId,
          newsletterName,
          newsletterContent
        });
      })
      .on("end", () => {
        fs.unlinkSync(filePath);
      });
    res.send('Newsletter sent.');
});

app.listen(port, () => console.log(`Hello world app listening on port ${port}!`));