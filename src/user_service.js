const config = require("../config/config");
const DB = require('../config/database');
const email_service = require('./email_service');
const { first, get, size, replace } = require("lodash");

class user_service {
  async checkUserExist (emailId) {
    let userExistBool = false;

    try {
      const DBObj = new DB();
      const findUserQuery = 'SELECT COUNT(*) as user_count FROM users WHERE email_id = ? AND status = 1';
      const {
        status,
        data
      } = await DBObj.executeQuery(findUserQuery, [ emailId ]);
      if (status === config.status.SUCCESS) {
        userExistBool = get(first(data), 'user_count', 0) ? true : false;
      }
    } catch (error) {
    }
    return userExistBool;
  }

  async addUser (firstname, lastname, emailId, age) {
    let response = {
      status: config.status.FAIL,
    };

    try {
      const isExist = await this.checkUserExist(emailId);
      if (!isExist) {
        const DBObj = new DB();
        const addUserQuery = 'INSERT INTO users (first_name, last_name, email_id, age) VALUES (?, ?, ?, ?)';
        const {
          status,
          data: addUserResponse
        } = await DBObj.executeQuery(
          addUserQuery,
          [ firstname, lastname, emailId, age ]
        );
        if (status === config.status.SUCCESS) {
          response["status"] = get(addUserResponse, 'affectedRows', 0) === 1
                                ? config.status.SUCCESS
                                : config.status.FAIL;
          response.message = response["status"] === config.status.SUCCESS
                                ? 'User has been added successfully.'
                                : 'Something went wrong.';
        }
      } else {
        response.message = 'User already exist.';
      }
    } catch (error) {
      response.msg = error;
    }
    return response;
  }

  async getNameFromEmailId (emailId) {
    let nameResponse = {
      status: config.status.FAIL,
      name: '',
      message: ''
    };

    try {
      const DBObj = new DB();
      const getUserQuery = 'SELECT first_name, last_name FROM users WHERE email_id = ? AND status = 1 LIMIT 1';
      const {
        status,
        data
      } = await DBObj.executeQuery(getUserQuery, [ emailId ]);
      if (status === config.status.SUCCESS) {
        if (size(data)) {
          nameResponse.name = `${get(first(data), 'first_name', '')} ${get(first(data), 'last_name', '')}`;
          nameResponse.status = config.status.SUCCESS;
        } else {
          nameResponse.message = 'User not exist.';
        }
      }
    } catch (error) {
      nameResponse.message = JSON.stringify(error);
    }
    return nameResponse;
  }
  
  async sendNewsletter(emailId, newsletterName, newsletterContent) {
    let response = {
      status: config.status.FAIL,
    };

    try {
      const { status, name, message } = await this.getNameFromEmailId(emailId);
      if (status === config.status.SUCCESS) {
        let newsletterContentWithName = newsletterContent;
        if (newsletterContentWithName.indexOf('{name}') > 0) {
          newsletterContentWithName = replace(newsletterContentWithName, '{name}', name);
        } else {
          newsletterContentWithName = `Hello ${name}, ${newsletterContentWithName}`;
        }
        const emailServiceObject = new email_service();
        const {
          status,
          message,
        } = await emailServiceObject.sendEmail(
          name,
          emailId,
          newsletterName,
          newsletterContentWithName
        );
        await this.addLog(
          emailId,
          newsletterName,
          status === config.status.SUCCESS ? 1 : 0,
          status === config.status.SUCCESS ? '' : message
        );
        response.status = status;
      } else {
        await this.addLog(
          emailId,
          newsletterName,
          0,
          message
        );
      }
    } catch (error) {
      response.msg = error;
    }
    return response;
  }

  async addLog (emailId, newsletterName, newsletterStatus, failureReason) {
    let response = {
      status: config.status.FAIL,
    };

    try {
      const DBObj = new DB();
      const addLogQuery = 'INSERT INTO logs (email_id, newsletter_name, status, failure_reason) VALUES (?, ?, ?, ?)';
      const {
        status,
        data: addLogResponse
      } = await DBObj.executeQuery(
        addLogQuery, 
        [ emailId, newsletterName, newsletterStatus, failureReason ]
      );
      if (status === config.status.SUCCESS) {
        response["status"] = get(addLogResponse, 'affectedRows', 0) === 1
                              ? config.status.SUCCESS
                              : config.status.FAIL;
        response.message = response["status"] === config.status.SUCCESS
                              ? 'Log has been recorded successfully.'
                              : 'Something went wrong.';
      }
    } catch (error) {
      response.msg = error;
    }
    return response;
  }
}

module.exports = user_service;
