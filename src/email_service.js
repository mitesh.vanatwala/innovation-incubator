require('dotenv').config()
const config = require("../config/config");
var SibApiV3Sdk = require('sib-api-v3-sdk');
SibApiV3Sdk.ApiClient.instance.authentications['api-key'].apiKey = process.env['SENDINBLUE_EMAIL_KEY'];

class email_service {
  async sendEmail (name, emailId, newsletterName, newsletterContent) {
    return new Promise(async (resolve, reject) => {
      const SENDER = {
        EMAILID: 'vmitesh.282@gmail.com',
        NAME: 'Mitesh Vanatwala'
      }
      let response = {
        status: config.status.FAIL,
        message: '',
      };

      new SibApiV3Sdk.TransactionalEmailsApi().sendTransacEmail(
        {
          'subject': newsletterName,
          'sender' : { 'email': SENDER.EMAILID, 'name': SENDER.NAME },
          'replyTo' : { 'email': SENDER.EMAILID, 'name': SENDER.NAME },
          'to' : [{'name': name, 'email': emailId }],
          'htmlContent' : `<html><head></head><body><p>${newsletterContent}</p></body></html>`,
        }
      ).then(function(data) {
        if (data.messageId !== '') {
          response.status = config.status.SUCCESS;
        }
        resolve(response);
      }, function(error) {
        response.message = JSON.stringify(error);
        resolve(response);
      });
    });
  }
}

module.exports = email_service;
